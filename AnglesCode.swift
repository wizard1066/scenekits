//
//  ContentView.swift
//  chess1
//
//  Created by localuser on 25.03.22.
//

import SwiftUI
import SceneKit
import Combine

let passDrag = PassthroughSubject<CGSize,Never>()
let updateView0 = PassthroughSubject<Int,Never>()
let resetLocation = PassthroughSubject<Void,Never>()
let turn90 = PassthroughSubject<Int,Never>()
let turn360 = PassthroughSubject<Void,Never>()
let flipFront2Back = PassthroughSubject<Void,Never>()

let lightX = PassthroughSubject<Bool,Never>()
let lightY = PassthroughSubject<Bool,Never>()
let lightZ = PassthroughSubject<Bool,Never>()
let lightIntensity = PassthroughSubject<Bool,Never>()

let changeX = PassthroughSubject<Float,Never>()
let changeY = PassthroughSubject<Float,Never>()
let changeZ = PassthroughSubject<Float,Never>()
let changeX2 = PassthroughSubject<Float,Never>()
let changeY2 = PassthroughSubject<Float,Never>()
let changeZ2 = PassthroughSubject<Float,Never>()

let pEuler = PassthroughSubject<Void,Never>()
let pEuler2 = PassthroughSubject<Void,Never>()


let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height

var sub2: AnyCancellable!
var sub1: AnyCancellable!
var sub3: AnyCancellable!
var sub4: AnyCancellable!
var sub5: AnyCancellable!

var turn90sub: AnyCancellable!
var turn360sub: AnyCancellable!
var flipF2Bsub: AnyCancellable!

var lightXsub: AnyCancellable!
var lightYsub: AnyCancellable!
var lightZsub: AnyCancellable!
var lightIntensitySub: AnyCancellable!


let fontSize:CGFloat = 48


struct ContentView: View {
    
    @State var xValue:Float = 0
    @State var idx = 0
    @State var view = 0
    
    let game1 = GameScene()
    let game2 = GameScene()
    

    @State var isReady = false
    @State var scene:SCNScene!
    @State var scene2:SCNScene!
    @State var cameraNode:SCNNode!
    @State var cameraNode2:SCNNode!
    
    @State var angleSub:AnyCancellable!
    @State var angleSub2:AnyCancellable!
    
    var body: some View {
        ZStack {
            HStack {
                if isReady {
                    GameView(scene: scene, cameraNode: cameraNode)
                    GameView(scene: scene2, cameraNode: cameraNode2)
                }
            }
            ZStack {
                
                HStack {
                    Text("X + Z + XYZ")
                        .font(Fonts.avenirNextCondensedBold(size: fontSize))
                        .frame(width: 640)
                        .padding(.top, 650)
//                    Text("Quaternions")
                    Text("Z + X + XYZ")
                       .font(Fonts.avenirNextCondensedBold(size: fontSize))
                       .frame(width: 640)
                       .padding(.leading, 60)
                       .padding(.top, 650)
//                    Text("Euler Angles")
//
//                        .font(Fonts.avenirNextCondensedBold(size: fontSize))
//                        .frame(width: 640)
//                        .padding(.leading, 60)
//                        .padding(.top, 650)
                }
            }
            ZStack {
                VStack {
                HStack {
                    Group {
                        Text("qX")
                            .font(Fonts.avenirNextCondensedBold(size: fontSize))
                            .foregroundColor(Color.red)
                            .onTapGesture(count: 2, perform: {
                                quartX2.send(-45)
                            })
                            .onTapGesture(count: 1, perform: {
                               //
                            })
                            .padding()
                        Text("qY")
                            .font(Fonts.avenirNextCondensedBold(size: fontSize))
                            .foregroundColor(Color.blue)
                            .onTapGesture(count: 2, perform: {
                                quartY2.send(-45)
                            })
                            .onTapGesture(count: 1, perform: {
                                //
                            })
                            .padding()
                        Text("qZ")
                            .font(Fonts.avenirNextCondensedBold(size: fontSize))
                            .foregroundColor(Color.green)
                            .onTapGesture(count: 2, perform: {
                                quartZ2.send(45)
                            })
                            .onTapGesture(count: 1, perform: {
                                //
                            })
                            .padding()
                        
                    }
                    Text("X")
                        .font(Fonts.avenirNextCondensedBold(size: fontSize))
                        .foregroundColor(Color.red)
                        .onTapGesture(count: 2, perform: {
                            changeX.send(45)
                        })
                        .onTapGesture(count: 1, perform: {
                            changeX2.send(45)
                        })
                        .padding()
                    Text("Y")
//                    Text("X2")
                        .font(Fonts.avenirNextCondensedBold(size: fontSize))
                        .foregroundColor(Color.blue)
                        .onTapGesture(count: 2, perform: {
                            changeY.send(45)
                        })
                        .onTapGesture(count: 1, perform: {
                            changeY2.send(45)
                        })
                        .padding()
                    Text("Z")
                        .font(Fonts.avenirNextCondensedBold(size: fontSize))
                        .foregroundColor(Color.green)
                        .onTapGesture(count: 2, perform: {
                            changeZ.send(45)
                        })
                        .onTapGesture(count: 1, perform: {
                            changeZ2.send(45)
                        })
                        .padding()
                    Text("P")
                        .font(Fonts.avenirNextCondensedBold(size: fontSize))
                        .foregroundColor(Color.blue)
                        .onTapGesture(count: 2, perform: {
                            pEuler.send()
                        })
                        .onTapGesture(count: 1, perform: {
                            pEuler2.send()
                        })
                        .padding()
                    Text("tX")
                        .font(Fonts.avenirNextCondensedBold(size: fontSize))
                        .foregroundColor(Color.red)
                        .onTapGesture(count: 2, perform: {
                            transX.send(45)
                        })
                        .onTapGesture(count: 1, perform: {
                            transX2.send(45)
                        })
                        .padding()
                    Text("tY")
                        .font(Fonts.avenirNextCondensedBold(size: fontSize))
                        .foregroundColor(Color.blue)
                        .onTapGesture(count: 2, perform: {
                            transY.send(45)
                        })
                        .onTapGesture(count: 1, perform: {
                            transY2.send(45)
                        })
                        .padding()
                    Text("tZ")
                        .font(Fonts.avenirNextCondensedBold(size: fontSize))
                        .foregroundColor(Color.green)
                        .onTapGesture(count: 2, perform: {
                            transZ.send(45)
                        })
                        .onTapGesture(count: 1, perform: {
                            transZ2.send(45)
                        })
                        .padding()
                    
                } // hstack
                Spacer()
                } // vstack
                
            }.onAppear {
                (scene2,cameraNode2) = game2.makeView(passX: changeX2, passY: changeY2, passZ: changeZ2, euler: pEuler2, passTx: transX2, passTy: transY2, passTz: transZ2, passQx: quartX2, passQy: quartY2, passQz: quartZ2)
                
                (scene,cameraNode) = game1.makeView(passX: changeX, passY: changeY, passZ: changeZ, euler:pEuler, passTx: transX, passTy: transY, passTz: transZ, passQx: quartX, passQy: quartY, passQz: quartZ)
                isReady = true
            }
            
            
        }
    }
}




struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


======== GameScene ========

//
//  GameScene.swift
//  chess1
//
//  Created by localuser on 25.03.22.
//

import SwiftUI
import SceneKit
import Combine

let pieceSize = 0.04
var coreNode: SCNNode!
var degrees:Float = 0

let angleX = PassthroughSubject<Float,Never>()
let angleY = PassthroughSubject<Float,Never>()
let angleZ = PassthroughSubject<Float,Never>()

let transX = PassthroughSubject<Float,Never>()
let transY = PassthroughSubject<Float,Never>()
let transZ = PassthroughSubject<Float,Never>()

let transX2 = PassthroughSubject<Float,Never>()
let transY2 = PassthroughSubject<Float,Never>()
let transZ2 = PassthroughSubject<Float,Never>()

let quartX = PassthroughSubject<Float,Never>()
let quartY = PassthroughSubject<Float,Never>()
let quartZ = PassthroughSubject<Float,Never>()

let quartX2 = PassthroughSubject<Float,Never>()
let quartY2 = PassthroughSubject<Float,Never>()
let quartZ2 = PassthroughSubject<Float,Never>()

class GameScene: SCNScene {
    static var shared = GameScene()
    
    var view: SCNView!
    var scene: SCNScene!
    var cameraNode: SCNNode!
    var cameraOrbit: SCNNode!
    
    var angleXSub: AnyCancellable!
    var angleYSub: AnyCancellable!
    var angleZSub: AnyCancellable!
    
    var transXSub: AnyCancellable!
    var transYSub: AnyCancellable!
    var transZSub: AnyCancellable!
    
    var quartXSub: AnyCancellable!
    var quartYSub: AnyCancellable!
    var quartZSub: AnyCancellable!
    
    var peuler: AnyCancellable!
    
    
    var cameraEuler = SCNVector3(x:0,y:0,z:0)
    typealias Pass = PassthroughSubject<Float,Never>
    
    func makeView(passX:Pass,passY:Pass,passZ:Pass,euler:PassthroughSubject<Void,Never>,passTx:Pass,passTy:Pass,passTz:Pass, passQx: Pass, passQy: Pass, passQz: Pass) -> (SCNScene,SCNNode) {
        let camera = SCNCamera()
        camera.fieldOfView = 90.0
        camera.orthographicScale = 9
        camera.zNear = 0
        camera.zFar = 1000
        let light = SCNLight()
        light.color = UIColor.white
        light.type = .area
        cameraNode = SCNNode()
        cameraNode.simdPosition = SIMD3<Float>(0.0, 0.0, 0.0)
        cameraNode.camera = camera
//        cameraNode.light = light
//        cameraNode.light?.color = UIColor.red
        
        cameraOrbit = SCNNode()
        cameraOrbit.position = SCNVector3(x: 0, y: 0, z: -4)
        cameraOrbit.eulerAngles.y = GLKMathDegreesToRadians(180)
//        cameraOrbit.simdPivot = simd_float4x4(.init(ix: 0,iy: 0,iz: 0,r: 0))
        
        print("camera \(cameraEuler)")
        
        
        cameraOrbit.addChildNode(cameraNode)
        
        scene = SCNScene()
        
        var pieces:[SkinNode] = []
        
        struct ChessSet {
            var resource: String
            var extern: String
            
            var tag: String
            var value: Int?
        }
        
        let piecex = [ChessSet(resource: "ROOK75k", extern: "usdz", tag: "colorRook",value: 4),
                       ChessSet(resource: "KNIGHT_75k", extern: "usdz", tag: "colorRook",value: 4),
                       ChessSet(resource: "BISHOP_75k", extern: "usdz",  tag: "colorRook",value: 4),
                       ChessSet(resource: "King75k", extern: "usdz",  tag: "colorRook",value: 4),
                       ChessSet(resource: "ROOK75k", extern: "usdz",  tag: "colorRook",value: 4),
                       ChessSet(resource: "BISHOP_75k", extern: "usdz",  tag: "colorRook",value: 4)]
                      
        for pieceux in piecex {
            let newURL = Bundle.main.url(forResource: pieceux.resource, withExtension: pieceux.extern)
            let newReferenceNode = SkinNode(x: nil, y: nil, tag: pieceux.tag, url: newURL)
            newReferenceNode?.load()
            newReferenceNode?.scale = SCNVector3(pieceSize, pieceSize, pieceSize)
            newReferenceNode?.name = "chess"
            newReferenceNode?.simdPivot.columns.3.y = 24
            newReferenceNode?.simdPivot.columns.3.x = 0
            newReferenceNode?.simdPivot.columns.3.z = 0
            pieces.append(newReferenceNode!)
        }
        
        scene?.rootNode.enumerateChildNodes { (node, stop) in
//            print("node \(node.name) \(node.scale)")
            
        }
        
        
//        scene.background.contents = UIColor.black
        
        view = SCNView()
        view.scene = scene
        
        view.pointOfView = cameraNode
        

        
        
        let coreNode = SCNNode()
        coreNode.position = SCNVector3(x: 0, y: 0, z: 0)
        
        
        
        
        
        
        let colors = [UIColor(red: 1, green: 1, blue: 1, alpha: 0.5), // front red
                      UIColor(red: 0, green: 0, blue: 200/255, alpha: 1), // blue
                      UIColor(red: 1, green: 100/255, blue: 0, alpha: 1), // orange
                      UIColor(red: 0, green: 100/255, blue: 0, alpha: 1), // right green
                      UIColor(red: 1, green: 1, blue: 1, alpha: 1),// back white
                      UIColor(red: 200/255, green: 200/255, blue: 0, alpha: 1), //yellow
                      ]
        
            let textMaterials = colors.map { color -> SCNMaterial in
                let material = SCNMaterial()
                material.diffuse.contents = color
                material.locksAmbientWithDiffuse = true
                return material
            }
        
        
        let newText = SCNText(string: "Alien", extrusionDepth: 0.02)
        newText.font = UIFont (name: "AvenirNextCondensed-Bold", size: 0.9)
//        newText.firstMaterial!.diffuse.contents = UIColor.white
        newText.materials = textMaterials
        newText.firstMaterial!.specular.contents = UIColor.white
        newText.firstMaterial?.isDoubleSided = true
        newText.chamferRadius = 0.01
        
        let planeNode = SCNNode(geometry: newText)
        planeNode.position = SCNVector3(0,0,2)
        
        let newText2 = SCNText(string: "Chess", extrusionDepth: 0.02)
        newText2.font = UIFont (name: "AvenirNextCondensed-Bold", size: 0.9)
//        newText2.firstMaterial!.diffuse.contents = UIColor.white
        newText2.materials = textMaterials
        newText2.firstMaterial!.specular.contents = UIColor.white
        newText2.firstMaterial?.isDoubleSided = true
        newText2.chamferRadius = 0.01
        
        let planeNode2 = SCNNode(geometry: newText2)
        planeNode2.position = SCNVector3(0,0,2)
        
        let (minBound, maxBound) = newText.boundingBox
        planeNode.pivot = SCNMatrix4MakeTranslation( (maxBound.x - minBound.x)/2 , minBound.y - 0.02, 0.02/2)
        planeNode.scale = SCNVector3Make(1, 1, 10)
        
        let (minBound2, maxBound2) = newText2.boundingBox
        planeNode2.pivot = SCNMatrix4MakeTranslation( (maxBound2.x - minBound2.x)/2 , maxBound2.y + 0.02, 0.02/2)
        planeNode2.scale = SCNVector3Make(1, 1, 10)
        
//        var pieces:[CustomRNode] = []
       
        
        
        
//  
        
       
        
        scene.rootNode.addChildNode(cameraOrbit)
        
        
      
        

        
        let cubeGeometry = SCNBox(width: 0.2, height: 0.2, length: 0.2, chamferRadius: 0.0)
        cubeGeometry.firstMaterial?.diffuse.contents = UIColor(red: 1, green: 1, blue: 1, alpha: 0.9)
        let square = SCNNode(geometry: cubeGeometry)
        
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.blue
        let sphere = SCNSphere(radius: 0.10)
        sphere.materials = [material]
        let sphereNode = SCNNode()
        sphereNode.geometry = sphere
        sphereNode.position = SCNVector3(0,1.2,0)
        
        var redNode = genCube(color: UIColor(red: 1, green: 0, blue: 0, alpha: 1), radius: 2.2)
        let blueNode = genCube(color: UIColor(red: 0, green: 0, blue: 1, alpha: 1), radius: 2.1)
        let greenNode = genCube(color: UIColor(red: 0, green: 1, blue: 0, alpha: 1), radius: 2.0)
        
        redNode.eulerAngles = SCNVector3(x: 0, y: 0, z: GLKMathDegreesToRadians(90))
        blueNode.eulerAngles = SCNVector3(x: 0, y: GLKMathDegreesToRadians(90), z:0)
        greenNode.eulerAngles = SCNVector3(x: GLKMathDegreesToRadians(90), y:0, z:0)
        
        coreNode.addChildNode(redNode)
        redNode.addChildNode(blueNode)
        blueNode.addChildNode(greenNode)
        greenNode.addChildNode(pieces[5])
        
        peuler = euler.sink(receiveValue: { [self] degree in
            print("euler ",redNode.eulerAngles,blueNode.eulerAngles,greenNode.eulerAngles)
        })
        
        quartXSub = passQx.sink(receiveValue: { [self] degree in
            print("angleQx")
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 4
            let quaternion = simd_quatf(angle: GLKMathDegreesToRadians(degree), axis: simd_float3(0,0,1))
            redNode.simdOrientation = quaternion * redNode.simdOrientation
            SCNTransaction.commit()
        })
        
        quartYSub = passQy.sink(receiveValue: { [self] degree in
            print("angleQy")
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 2
            let quaternion = simd_quatf(angle: GLKMathDegreesToRadians(degree), axis: simd_float3(1,0,0))
            blueNode.simdOrientation = quaternion * blueNode.simdOrientation
            SCNTransaction.commit()
        })
        
        quartZSub = passQz.sink(receiveValue: { [self] degree in
            print("angleQz")
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 2
            let quaternion = simd_quatf(angle: GLKMathDegreesToRadians(degree), axis: simd_float3(0,1,0))
            greenNode.simdOrientation = quaternion * greenNode.simdOrientation
            SCNTransaction.commit()
        })
        
        
        angleXSub = passX.sink(receiveValue: { [self] degree in
            print("angleX")
            let actionZ = SCNAction.rotate(by: CGFloat(GLKMathDegreesToRadians(degree)), around: SCNVector3(x: 0, y: 0, z: 1), duration: 2)
            let actionX = SCNAction.rotate(by: CGFloat(GLKMathDegreesToRadians(degree)), around: SCNVector3(x: 1, y: 0, z: 0), duration: 1)
            let actionQ = SCNAction.rotate(by: CGFloat(GLKMathDegreesToRadians(-degree)), around: SCNVector3(x: 1, y: 1, z: 1), duration: 2)
            let group = SCNAction.sequence([actionZ,actionX,actionQ])
            redNode.runAction(group)
//            redNode.runAction(actionZ)
        })
        
        angleYSub = passY.sink(receiveValue: { [self] degree in
            print("angleY")
            let actionX = SCNAction.rotate(by: CGFloat(GLKMathDegreesToRadians(degree)), around: SCNVector3(x: 1, y: 0, z: 0), duration: 2)
            let actionZ = SCNAction.rotate(by: CGFloat(GLKMathDegreesToRadians(degree)), around: SCNVector3(x: 0, y: 0, z: 1), duration: 1)
            let actionQ = SCNAction.rotate(by: CGFloat(GLKMathDegreesToRadians(-degree)), around: SCNVector3(x: 1, y: 1, z: 1), duration: 2)
            
            let group = SCNAction.sequence([actionX,actionZ,actionQ])
            redNode.runAction(group)
//            blueNode.runAction(actionX)
        })
        
        angleZSub = passZ.sink(receiveValue: { [self] degree in
            print("angleZ")
            let actionY = SCNAction.rotate(by: CGFloat(GLKMathDegreesToRadians(degree)), around: SCNVector3(x: 0, y: 1, z: 0), duration: 2)
            greenNode.runAction(actionY)
        })
        
        transXSub = passTx.sink(receiveValue: { [self] degree in
            print("angleTx")
            let xAngle = SCNMatrix4MakeRotation(GLKMathDegreesToRadians(degree), 0, 0, 1)
            let yAngle = SCNMatrix4MakeRotation(GLKMathDegreesToRadians(0), 0, 0, 0)
            let zAngle = SCNMatrix4MakeRotation(GLKMathDegreesToRadians(0), 0, 0, 0)
            
            let rotationMatrix = SCNMatrix4Mult(SCNMatrix4Mult(xAngle, yAngle), zAngle)

            
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 2
            SCNTransaction.completionBlock = {
                print("redNode \(redNode.eulerAngles)")
            }

            redNode.transform = SCNMatrix4Mult(rotationMatrix, redNode.transform)
            SCNTransaction.commit()
        })
        
        transYSub = passTy.sink(receiveValue: { [self] degree in
            print("angleTy")
//            redNode = transX(angle: degree, node: redNode)
            let xAngle = SCNMatrix4MakeRotation(GLKMathDegreesToRadians(0), 0, 0, 0)
            let yAngle = SCNMatrix4MakeRotation(GLKMathDegreesToRadians(degree), 0, 0, 1)
            let zAngle = SCNMatrix4MakeRotation(GLKMathDegreesToRadians(0), 0, 0, 0)
            
            var rotationMatrix = SCNMatrix4Mult(SCNMatrix4Mult(xAngle, yAngle), zAngle)
            
            SCNTransaction.begin()
                // Set the duration and the completion block
            SCNTransaction.animationDuration = 2
            SCNTransaction.completionBlock = {
                print("blueNode \(redNode.eulerAngles)")
            }

            // Set the new transform
            blueNode.transform = SCNMatrix4Mult(rotationMatrix, blueNode.transform)

            SCNTransaction.commit()
            
        })
        
        transZSub = passTz.sink(receiveValue: { [self] degree in
            print("angleTz")
//            redNode = transX(angle: degree, node: redNode)
            let xAngle = SCNMatrix4MakeRotation(GLKMathDegreesToRadians(0), 0, 0, 0)
            let yAngle = SCNMatrix4MakeRotation(GLKMathDegreesToRadians(0), 0, 0, 0)
            let zAngle = SCNMatrix4MakeRotation(GLKMathDegreesToRadians(-degree), 0, 0, 1)
            
            var rotationMatrix = SCNMatrix4Mult(SCNMatrix4Mult(xAngle, yAngle), zAngle)

            SCNTransaction.begin()
                // Set the duration and the completion block
            SCNTransaction.animationDuration = 2
            SCNTransaction.completionBlock = {
                print("greenNode \(redNode.eulerAngles)")
            }

            // Set the new transform
            greenNode.transform = SCNMatrix4Mult(rotationMatrix, greenNode.transform)

            SCNTransaction.commit()
            
        })
        
        
        
        
        scene.rootNode.addChildNode(coreNode)
        
        return (scene, cameraNode)
    }
    
    
    
    
    
    func genCube(color: UIColor, radius: Double) -> SCNNode {
        let newNode = SCNNode()
        newNode.position = SCNVector3(x: 0, y: 0, z: 0)
        let center = CGPoint(x: 0, y: 0)
        let angle = 0
        let sides = 360
        
        var cords:[CGPoint] = []
        for i in stride(from: angle, to: (359 + angle), by: 360/sides) {
                  let radians = Double(i) * Double.pi / 180.0
                  let x = Double(center.x) + radius * cos(radians)
                  let y = Double(center.y) + radius * sin(radians)
                  cords.append(CGPoint(x: x, y: y))
                }
        
        for i in 0..<cords.count {
            
            let cubeGeometry = SCNBox(width: 0.01, height: 0.01, length: 0.01, chamferRadius: 0)
            cubeGeometry.firstMaterial?.diffuse.contents = color
            let square = SCNNode(geometry: cubeGeometry)
            square.position = SCNVector3(cords[i].x, cords[i].y, 0)

            newNode.addChildNode(square)
            
//            let material = SCNMaterial()
//            material.diffuse.contents = color
//
//            let sphereGeometry = SCNSphere(radius: 0.025)
//            sphereGeometry.firstMaterial?.diffuse.contents = color
//
//            let sphere = SCNNode(geometry: sphereGeometry)
//            sphere.position = SCNVector3(cords[i].x, cords[i].y, 0)
//
//            newNode.addChildNode(sphere)
        }
        return newNode
    }
    
    func setCamera(translation: CGSize) {
       
        let dx = cameraOrbit.eulerAngles.x
        let dy = cameraOrbit.eulerAngles.y
        
        let scrollWidthRatio = Float(translation.width / screenWidth) / 16
        let scrollHeightRatio = Float(translation.height / screenHeight) / 16
        cameraOrbit.eulerAngles.y = Float(-2 * Float.pi) * scrollWidthRatio + dy
        cameraOrbit.eulerAngles.x = Float(-Float.pi) * scrollHeightRatio + dx
        
    }
    
    func setCameraV(translation: CGSize) {
//        let dx = coreNode.eulerAngles.x
        let dy = coreNode.eulerAngles.y
//        let translation = settings.returnTranslation()
        let scrollWidthRatio = Float(translation.width / screenWidth) / 4
        let scrollHeightRatio = Float(translation.height / screenHeight) / 4
        coreNode.eulerAngles.y = Float(2 * Float.pi) * scrollWidthRatio + dy
//        coreNode.eulerAngles.x = Float(Float.pi) * scrollHeightRatio + dx
        
        let yAngle = abs(GLKMathRadiansToDegrees(coreNode.eulerAngles.y)).truncatingRemainder(dividingBy:360)
        let zAngle = abs(GLKMathRadiansToDegrees(coreNode.eulerAngles.z)).truncatingRemainder(dividingBy:360)
        let xAngle = abs(GLKMathRadiansToDegrees(coreNode.eulerAngles.x)).truncatingRemainder(dividingBy:360)
        
        if (yAngle > 315 || yAngle < 45) || yAngle > 135 && yAngle < 225 {
            print("here")
            coreNode.eulerAngles.z = 0
            let dx = coreNode.eulerAngles.x
            coreNode.eulerAngles.x = Float(Float.pi) * scrollHeightRatio + dx
//            coreNode.eulerAngles.z = GLKMathDegreesToRadians(45)
        } else {
//            coreNode.eulerAngles.x = 0
            let dz = coreNode.eulerAngles.z
            coreNode.eulerAngles.x = Float(Float.pi) * scrollWidthRatio + dz
            coreNode.eulerAngles.y = GLKMathDegreesToRadians(45)
        }

        
        
//        print("y \(yAngle) x \(xAngle) z \(zAngle) ")
    }
}
