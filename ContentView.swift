import SwiftUI
import SceneKit
import Combine

struct ContentView: View {
    var body: some View {
        GameView()
    }
}

let hRotate = PassthroughSubject<(Float,Int),Never>()
let vRotate = PassthroughSubject<(Float,Int),Never>()
let zRotate = PassthroughSubject<(Float,Int),Never>()

struct GameView: View {
    
    
    
    var (scene,cameraNode) = GameScene.shared.defineScene()
    @State var index = 0
    
    var body: some View {
        SceneView(
            scene: scene,
            pointOfView: cameraNode,
            options: [.autoenablesDefaultLighting, .rendersContinuously], delegate: SceneDelegate())
            .border(Color.red)
            .onReceive(hRotate, perform: { value in
                let (dees,din) = value
                GameScene.shared.rotateCubeH(degrees: dees, nix: din)
            })
            .onReceive(vRotate, perform: { value in
                let (dees,din) = value
                GameScene.shared.rotateCubeV(degrees: dees, nix: din)
            })
            .onReceive(zRotate, perform: { value in
                let (dees,din) = value
                GameScene.shared.rotateCubeZ(degrees: dees, nix: din)
            })
            .onAppear(perform: {
                Task(priority: .userInitiated) {
                    animateV()
                }
            })
    }
    
    @MainActor func animateV() {
        let angles = [45,45,-180,-45,-45,-45,-45,360,45,45]
        let aux = [0,0,2,0,0,0,0,4,0,9]
        reanimate(index: index, angle: angles, aux: aux)
    }
    
    @MainActor
    func reanimate(index: Int, angle:[Int], aux:[Int]) {
        print("reanimate \(angle) \(aux)")
        Task { try! await Task.sleep(seconds: 2.1)
            hRotate.send((Float(angle[index]),aux[index]))
            if aux[index] != 9 {
                let nix = index + 1
                reanimate(index: nix,angle: angle, aux: aux)
            }
        }
    }
}


class GameScene {
    
    var scene = SCNScene(named: "MyScene.scn")
    
    var planeNode: SCNNode!
    var cameraNode = SCNNode()
    
    static var shared = GameScene()
    
    func defineScene() -> (SCNScene,SCNNode) {
        
        planeNode = SCNNode(geometry: SCNPlane(width: 4.5, height: 4.5))
        planeNode.geometry?.firstMaterial?.diffuse.contents = UIImage(named: "paper")
        
        scene?.rootNode.addChildNode(planeNode)
        
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3(x: 0, y: 0, z: 6)
        cameraNode.name = "cameraNode"
        
        scene?.rootNode.addChildNode(cameraNode)
        
        addLightNode(xAxis: -4, yAxis: -4, zAxis: -4)
        addLightNode(xAxis: 4, yAxis: 4, zAxis: 4)
        addLightNode(xAxis: 4, yAxis: 4, zAxis: -4)
        addLightNode(xAxis: -4, yAxis: -4, zAxis: 4)
        
        return (scene!,cameraNode)
    }
    
    func addLightNode(xAxis: Float, yAxis: Float, zAxis: Float) {
        let light4 = SCNLight()
        light4.type = SCNLight.LightType.omni
        let lightNode4 = SCNNode()
        lightNode4.light = light4
        lightNode4.position = SCNVector3(x: xAxis, y: yAxis, z: zAxis)
        scene?.rootNode.addChildNode(lightNode4)
    }
    
    func rotateCubeH(degrees:Float, nix:Int) {
//        print("rotateCubeH \(planeNode.eulerAngles) \(degrees)")
        
        if nix == 2 {
            // flip image
            planeNode.scale = SCNVector3Make(-1,1,1)
        }
        if nix == 4 {
            // flip back
            planeNode.eulerAngles = SCNVector3(0, GLKMathDegreesToRadians(-90), 0)
            planeNode.scale = SCNVector3Make(1,1,1)
        }
        if nix == 6 {
            // reset angles
            planeNode.eulerAngles = SCNVector3(0, 0, 0)
        }
        
        if nix == 8 {
            // reset material
            planeNode.geometry?.firstMaterial?.diffuse.contents = SCNMatrix4Identity
        }
        
        let oldTransform = planeNode.transform
        let rotation = SCNMatrix4MakeRotation(GLKMathDegreesToRadians(degrees), 0, 1, 0)
        SCNTransaction.begin()
        SCNTransaction.animationDuration = 0.2
        planeNode.transform = SCNMatrix4Mult(rotation, oldTransform)
        SCNTransaction.commit()
    }
    
    func rotateCubeV(degrees:Float, nix:Int) {
        print("rotateCubeV \(planeNode.eulerAngles) \(degrees)")
        
        let oldTransform = planeNode.transform
        let rotation = SCNMatrix4MakeRotation(GLKMathDegreesToRadians(degrees), 1, 0, 0)
        SCNTransaction.begin()
        SCNTransaction.animationDuration = 0.5
        planeNode.transform = SCNMatrix4Mult(rotation, oldTransform)
        SCNTransaction.commit()
    }
    
    func rotateCubeZ(degrees:Float, nix:Int) {
        print("rotateCubeZ \(planeNode.eulerAngles) \(degrees)")
        
        let oldTransform = planeNode.transform
        let rotation = SCNMatrix4MakeRotation(GLKMathDegreesToRadians(degrees), 0, 0, 1)
        SCNTransaction.begin()
        SCNTransaction.animationDuration = 0.5
        planeNode.transform = SCNMatrix4Mult(rotation, oldTransform)
        SCNTransaction.commit()
    }
}

class SceneDelegate: NSObject, SCNSceneRendererDelegate {
    var count = 0
    
    @MainActor
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
    }
}

extension Task where Success == Never, Failure == Never {
    static func sleep(seconds: Double) async throws {
        let duration = UInt64(seconds * 1000_000_000)
        try await sleep(nanoseconds: duration)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}